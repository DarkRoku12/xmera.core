﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xmera.Libraries;

namespace Xmera.SEC.Controllers
{
    [ApiController]
    [Route( "[controller]" )]
    public class TokenController : ControllerBase
    {
        [HttpGet]
        public String Get( int role )
        {
            // Asumimos que tenemos un usuario válido.
            var user = new
            {
                Name = "Enmanuel Reynoso",
                Email = "ereynoso@itla.edu.do",
                Id = 12,
                Role = role,
            };

            // Leemos el secret_key desde nuestro appseting.
            var key = Encoding.ASCII.GetBytes( Env.XMERA_JWT_KEY );

            // Creamos los claims (pertenencias, características) del usuario
            var claims = new Dictionary<string,object>()
            {
                ["Id"] = user.Id,
                ["Name"] = user.Name,
                ["Email"] = user.Email,
                ["Role"] = user.Role,
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Claims = claims,
                // Nuestro token va a durar un día
                Expires = DateTime.UtcNow.AddDays( 1 ),
                // Credenciales para generar el token usando nuestro secretykey y el algoritmo hash 256
                SigningCredentials = new SigningCredentials( new SymmetricSecurityKey( key ), SecurityAlgorithms.HmacSha256Signature )
            };
            
            var tokenHandler = new JwtSecurityTokenHandler(); 
            var createdToken = tokenHandler.CreateToken( tokenDescriptor );
            return tokenHandler.WriteToken( createdToken );
        }
    }
}
