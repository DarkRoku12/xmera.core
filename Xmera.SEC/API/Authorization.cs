﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Xmera.Libraries;

namespace Xmera.SEC.API
{
    using MSN = Libraries.Shortnames.Modules;

    public class PermissionsAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        private readonly ScreensId _required_screen_id;
        private readonly Permission _required_permission;

        // Rol -> [ [pantalla] = [access code] ]
        private static readonly Dictionary<int, Dictionary<ScreensId, Permission>> _cache = new Dictionary<int, Dictionary<ScreensId, Permission>>();

        private static int ToInt( string id )
        {
            try { return int.Parse( id ); }
            catch { return default; }
        }

        public PermissionsAttribute( ScreensId screenId, Permission permission )
        {
            _required_screen_id = screenId;
            _required_permission = permission;
        }

        public void OnAuthorization( AuthorizationFilterContext context )
        {
            Dictionary<ScreensId, Permission> screenPermissionsList; // Cached ScreenId+Permissions
            Permission rolePermissionCode; // Access code for the specified role+screen.

            var roleId = ToInt( context.HttpContext.User.Claims.First( c => c.Type == "Role" ).Value );

            // Look-up the role permission list, if not present search on database and add it to the cache.
            if(!_cache.TryGetValue( roleId, out screenPermissionsList ))
            {
                // Simulate role searching + adding.

                // Testing: Add role 1 -> Developer.
                _cache[1] = new Dictionary<ScreensId, Permission>()
                {
                    [ScreensId.Dashboard] = Permission.ReadWriteDelete,
                    [ScreensId.Clients] = Permission.ReadWriteDelete,
                    [ScreensId.Users] = Permission.ReadWriteDelete,
                };

                // Testing: Add role 7 -> Contable.
                _cache[7] = new Dictionary<ScreensId, Permission>()
                {
                    [ScreensId.Dashboard] = Permission.Read,
                    [ScreensId.Clients] = Permission.Read,
                    [ScreensId.Users] = Permission.None,
                };
            }

            // If the role permission list was not present in the cache, look for it again.
            if(screenPermissionsList == null && !_cache.TryGetValue( roleId, out screenPermissionsList ))
            {
                // Role do not exist => forbid.
                StdLogger.Log.Info( MSN.Core, MSN.SEC, "[Authorize for endpoint] Role does not exists: $$", new
                {
                    roleId = roleId,
                } );

                context.Result = new ForbidResult();
                return;
            }

            // Get the permission code of the role for the specified screen.
            if(!screenPermissionsList.TryGetValue( this._required_screen_id, out rolePermissionCode ))
            {
                // ScreenId does not exist within the role.
                StdLogger.Log.Info( MSN.Core, MSN.SEC, "[Authorize for endpoint] ScreenId does not exist for this role: $$", new
                {
                    screenId = this._required_screen_id,
                    roleId = roleId,
                } );

                context.Result = new ForbidResult();
                return;
            }

            // Validate accessCode with permission.
            if(Libraries.Access.IsAllowed( rolePermissionCode, this._required_permission ))
            {
                StdLogger.Log.Info( MSN.Core, MSN.SEC, "[Authorize for endpoint] Permission granted: $$", new
                {
                    screenId = this._required_screen_id,
                    roleId = roleId,
                    needed = this._required_permission,
                    have = rolePermissionCode,
                } );
            }
            else // Access not allowed.
            {
                StdLogger.Log.Info( MSN.Core, MSN.SEC, "[Authorize for endpoint] Permission denied: $$", new
                {
                    screenId = this._required_screen_id,
                    roleId = roleId,
                    needed = this._required_permission,
                    have = rolePermissionCode,
                } );
                context.Result = new ForbidResult();
            }
        }
    }

    // TODO: Modules outside Xmera.Core will use this instead of PermissionsAttribute.
    // https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters?view=aspnetcore-5.0#authorization-filters
    // https://stackoverflow.com/questions/43979889/async-onactionexecuting-in-asp-net-cores-actionfilterattribute
}
