﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xmera.Libraries;
using Xmera.SEC.API;

namespace Xmera.RRHH.Controllers
{
    [ApiController]
    [Route( "[controller]" )]
    public class TestController : XmeraBaseController
    {
        [HttpGet]
        [Authorize]
        [Route( "Test1" )]
        public String Test()
        {
            return $"Probando validación de token desde módulo RRHH: " +
                $"{TokenContext.Id} {TokenContext.Role} {TokenContext.Name} {TokenContext.Email}";
        }

        [HttpGet]
        [Authorize]
        [Permissions( ScreensId.Users, Permission.Read )]
        [Route( "TestScreenAccess/Users" )]
        public String TestScreenUserAccess()
        {
            return "Permitido: Screen usuarios";
        }

        [HttpGet]
        [Authorize]
        [Permissions( ScreensId.Dashboard, Permission.Read )]
        [Route( "TestScreenAccess/Dashboard" )]
        public String TestScreenDashboardAccess()
        {
            return "Permitido: Screen dashboard";
        }

        [HttpGet]
        [ExPermission]
        [Route( "TestAsyncAuth" )]
        public String TestAsyncAuth()
        {
            return "Permitido: Async auth";
        }
    }
}
