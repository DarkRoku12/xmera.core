using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xmera.Libraries;

namespace Xmera.Core
{
    using MSN = Xmera.Libraries.Shortnames.Modules;

    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        public Startup( IConfiguration configuration )
        {
            Configuration = configuration;

            var unset = Env.GetUnsetEnvVars();

            // Xmera.Global.Env.

            /*
            StdLogger.Log.Debug( 
                MSN.Core, 
                MSN.RRHH, 
                "Saludado $$ desde $$ el $$ logging $$ ",
                new { user = "Enmanuel"  },
                "esto es una string"
                , false ,
                null );
            */
            if(unset.Any()) StdLogger.Log.Warn( MSN.Core, MSN.SEC, "Hay variables de entorno sin establecerse: $$", unset.ToList() );
            /*
            log.Debug( "Core" , "HHRR" , "this is a debug message $$" , new { x = 10, y = 20 } );
            log.Info( "Core", "SEC", "this is a info message $$",null );
            log.Warn( "Core", "HHRR", "this is a Warn message $$", new { name = "Enma" , age = 18, valid = true } );
            log.Error( "Core", "SEC", "this is a Error message $$", "hola :]" );
            */
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices( IServiceCollection services )
        {
            services.AddControllers();

            var key = Encoding.ASCII.GetBytes( Env.XMERA_JWT_KEY );

            services.AddAuthentication( x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            } ).AddJwtBearer( x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey( key ),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            } );

            services.AddSwaggerGen( swagger =>
            {
                //This is to generate the Default UI of Swagger Documentation    
                swagger.SwaggerDoc( "v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Xmera.Core",
                    Description = "Xmera.Core API",
                } );
                // To Enable authorization using Swagger (JWT)    
                swagger.AddSecurityDefinition( "Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Enter 'Bearer' [space] and then your valid token in the text input below.\r\n\r\nExample: \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"",
                } );
                swagger.AddSecurityRequirement( new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[]{ }
                    }
                } );
            } );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IWebHostEnvironment env )
        {
            System.Diagnostics.Debug.WriteLine( "Configuring: Core" );

            string os = Environment.GetEnvironmentVariable( "OS" );
            System.Diagnostics.Debug.WriteLine( "System: " + os );

            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI( c => c.SwaggerEndpoint( "/swagger/v1/swagger.json", "Xmera.Core v1" ) );
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization(); 

            app.UseEndpoints( endpoints =>
             {
                 endpoints.MapControllers();
             } );
        }
    }
}
