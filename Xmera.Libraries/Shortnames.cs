﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xmera.Libraries.Shortnames
{
    /// <include file='docs/Shortnames.xml' path='ClassModule/docs'/>
    public static class Modules
    {
        /// <summary> Módulo: Recursos Humanos. </summary> 
        public static readonly string Core = "CORE";

        /// <summary> Módulo: Recursos Humanos. </summary> 
        public static readonly string RRHH = "RRHH";

        /// <summary> Módulo: Seguridad. </summary> 
        public static readonly string SEC = "SEC";

        /// <summary> Módulo: Seguridad. </summary> 
        public static readonly string Security = "SEC";
    }
}
