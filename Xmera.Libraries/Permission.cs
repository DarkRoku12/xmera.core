﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Xmera.Libraries
{
    /// <summary> Listado de permisos globales para Xmera </summary>
    [Flags]
    public enum Permission
    {
        // Basic.
        None = 0,
        Read = 1 << 0,
        Write = 1 << 1,
        Delete = 1 << 2,

        // Combined.
        ReadWrite = Read | Write,
        ReadWriteDelete = ReadWrite | Delete,
    }

    /// <summary> Listado de ids de pantallas globales para Xmera </summary>
    public enum ScreensId
    {
        Dashboard = 1,
        Users = 2,
        Clients = 3,
    }

    public static class Access
    {
        /// <summary> ¿Los permisos del rol contienen todos los permisos solicitados? </summary>
        public static bool IsAllowed( int rolePermission, params Permission[] permissions )
        {
            var needed = 0;
            foreach(var permission in permissions)
                needed |= (int)permission;
            return (rolePermission & needed) == needed;
        }

        /// <summary> ¿Los permisos del rol contienen todos los permisos solicitados? </summary>
        public static bool IsAllowed( Permission rolePermission, params Permission[] permissions )
        {
            return IsAllowed( (int)rolePermission, permissions );
        }
    }
}
