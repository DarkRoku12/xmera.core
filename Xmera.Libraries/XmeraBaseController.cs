﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xmera.Libraries
{
    public class JwtTokenContext
    {
        public readonly int Id;
        public readonly int Role;
        public readonly string Name;
        public readonly string Email;

        public JwtTokenContext( int Id, int Role, string Name, string Email )
        {
            this.Id = Id;
            this.Role = Role;
            this.Name = Name;
            this.Email = Email;
        }
    }

    public class XmeraBaseController : ControllerBase
    {
        private static int ToInt( string id )
        {
            try { return int.Parse( id ); }
            catch { return default; }
        }

        public JwtTokenContext TokenContext
        {
            get
            {
                if(this.HttpContext == null) throw new Exception( "HttpContext MUST be used inside a route method" );
                return new JwtTokenContext(
                    Id: ToInt( this.HttpContext.User.Claims.First( c => c.Type == "Id" ).Value ),
                    Role: ToInt( HttpContext.User.Claims.First( c => c.Type == "Role" ).Value ),
                    Name: HttpContext.User.Claims.First( c => c.Type == "Name" ).Value,
                    Email: HttpContext.User.Claims.First( c => c.Type == "Email" ).Value
                );
            }
        }

        public XmeraBaseController()
        {
            /* HttpContext is null inside the controller constructor.
             * HttpContext MUST be used inside a route method. 
             */
        }
    }
}
