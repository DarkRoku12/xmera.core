﻿using System;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Utf8Json;

namespace Xmera.Libraries
{
    /// <include file='docs/Logger.xml' path='ClassLogger/docs'/>
    public class Logger
    {
        /// <include file='docs/Logger.xml' path='ClassLogger/EnumLevel'/>
        public enum Level
        {
            Debug,
            Info,
            Warning,
            Error,
        }

        /// <include file='docs/Logger.xml' path='ClassLogger/ClassLog/docs'/>
        public class Log
        {
            /// <include file='docs/Logger.xml' path='ClassLogger/ClassLog/Members/Level'/>
            public Level Level { get; set; }
            /// <include file='docs/Logger.xml' path='ClassLogger/ClassLog/Members/Module'/>
            public string Module { get; set; }
            /// <include file='docs/Logger.xml' path='ClassLogger/ClassLog/Members/Submodule'/>
            public string Submodule { get; set; }
            /// <include file='docs/Logger.xml' path='ClassLogger/ClassLog/Members/Message'/>
            public string Message { get; set; }
        }

        /// <include file='docs/Logger.xml' path='ClassLogger/DelegateCallback'/>
        public delegate void Callback( Log log );

        /// <include file='docs/Logger.xml' path='ClassLogger/MethodFormat'/>
        public static string Format( string message, object[] values )
        {
            // Handle null values formatting.
            if(values == null) values = new object[] { null };

            // Handle empty list -> no formatting at all.
            if(values.Length < 1) return message;

            var index = 0;

            return Regex.Replace( message, @"\$\$", m =>
            {
                try
                {
                    return JsonSerializer.ToJsonString( values[index++] );
                }
                catch
                {
                    return "";
                }
            } );
        }

        /// <include file='docs/Logger.xml' path='ClassLogger/MethodLogLevel'/>
        public static string LogLevel( string level, string module, string submodule, string message )
        {
            return $"[{module}.{submodule}][{level}]: {message}";
        }

        /// <include file='docs/Logger.xml' path='ClassLogger/MethodDebug'/>
        public void Debug( string module, string submodule, string message, params object[] values )
        {
            var log = LogLevel( "Debug", module, submodule, Format( message, values ) );

            _callback( new Log
            {
                Level = Level.Debug,
                Module = module,
                Submodule = submodule,
                Message = log,
            } );
        }

        /// <include file='docs/Logger.xml' path='ClassLogger/MethodInfo'/>
        public void Info( string module, string submodule, string message, params object[] values )
        {
            var log = LogLevel( "Info", module, submodule, Format( message, values ) );

            _callback( new Log
            {
                Level = Level.Info,
                Module = module,
                Submodule = submodule,
                Message = log,
            } );
        }

        /// <include file='docs/Logger.xml' path='ClassLogger/MethodWarn'/>
        public void Warn( string module, string submodule, string message, params object[] values )
        {
            var log = LogLevel( "Warning", module, submodule, Format( message, values ) );

            _callback( new Log
            {
                Level = Level.Warning,
                Module = module,
                Submodule = submodule,
                Message = log,
            } );
        }

        /// <include file='docs/Logger.xml' path='ClassLogger/MethodError'/>
        public void Error( string module, string submodule, string message, params object[] values )
        {
            var log = LogLevel( "Error", module, submodule, Format( message, values ) );

            _callback( new Log
            {
                Level = Level.Error,
                Module = module,
                Submodule = submodule,
                Message = log,
            } );
        }

        private readonly Callback _callback;

        /// <include file='docs/Logger.xml' path='ClassLogger/docs'/>
        public Logger( Callback callback )
        {
            this._callback = callback;
        }
    }

    public static class StdLogger
    {
        private static Logger _logger;
        private static object _locker = new object();

        private static void StdCallback( Logger.Log log )
        {
            Debug.WriteLine( log.Message );
            //if( log.Level == Logger.Level.Error )
            //if( log.Module = MSN.SEC ) 
        }

        public static Logger Log 
        {
            get
            {
                if( _logger != null ) return _logger;

                lock( _locker )
                {
                    if( _logger != null ) return _logger;

                    _logger = new Logger( StdCallback );
                }

                return _logger;
            }
        }
    }
}
