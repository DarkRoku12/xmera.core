﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Xmera.Libraries
{
    public static class Env
    {
        /// <summary> Working environment type. </summary>
        public enum EnvType
        {
            Development, Quality, Production
        }

        /// <summary> Mark an attribute to be checked in the methpd: GetUnsetEnvVars. </summary>
        /// <seealso cref="Env.GetUnsetEnvVars"/> 
        [AttributeUsage( AttributeTargets.Property )]
        private class CheckIfSet : Attribute { }

        /// <summary> Current running operative system. </summary>
        /// <remarks> Example return string: <c> WINDOWS_NT. </c> </remarks>
        [CheckIfSet]
        public static string OS => Environment.GetEnvironmentVariable( "OS" );

        /// <summary> Get the current working environment. </summary> 
        public static EnvType XMERA_ENV
        {
            get
            {
                var _env = Environment.GetEnvironmentVariable( "OS" ).ToUpper();

                return _env switch
                {
                    "PRODUCTION" => EnvType.Production,
                    "QUALITY" => EnvType.Quality,
                    _ => EnvType.Development, // DEVELOPMENT as default.
                };
            }
        }

        /// <summary> Connection string to XMERA.CORE Database. </summary> 
        [CheckIfSet]
        public static string XMERA_CORE_DB => Environment.GetEnvironmentVariable( "XMERA_CORE_DB" );

        /// <summary> XMERA.CORE IP/URL. </summary> 
        [CheckIfSet]
        public static string XMERA_CORE_URL => Environment.GetEnvironmentVariable( "XMERA_CORE_URL" );

        /// <summary> XMERA SECRET KEY FOR JWT TOKENS. </summary>
        [CheckIfSet]
        public static string XMERA_JWT_KEY => Environment.GetEnvironmentVariable( "XMERA_JWT_KEY" );

        /// <summary> Get a list of [needed] Environment variable that where not set. </summary>
        public static string[] GetUnsetEnvVars()
        {
            var props = typeof( Env ).GetProperties().Where(
                m => m.CustomAttributes.Any(
                    a => a.AttributeType == typeof( CheckIfSet )
                )
            );

            var nonSet = new List<string>();

            foreach(var prop in props)
            {
                try
                {
                    var value = (string)prop.GetValue( null );
                    if(string.IsNullOrEmpty( value ))
                        nonSet.Add( prop.Name );
                }
                catch( Exception e )
                {
                    Debug.WriteLine( e );
                }
            }

            return nonSet.ToArray();
        }
    }
}
