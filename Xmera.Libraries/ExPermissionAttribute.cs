﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Xmera.Libraries
{
    public class ExPermissionAttribute : ActionFilterAttribute, IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync( AuthorizationFilterContext context )
        {
            await (Task.FromResult( true ));
            System.Diagnostics.Debug.WriteLine( "Using AuthAttribute with Async." );
            // throw new NotImplementedException();
            // use context.Result = new ForbidResult(); to throw 403.
            return;
        }
    }
}
